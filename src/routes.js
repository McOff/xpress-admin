import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from 'App';
import Shedule from 'Shedule';
import Clients from 'Clients';
import Day from 'Day';
import Week from 'Week';

export default (
  <Route path='/admin' component={App} >
    <IndexRoute component={Shedule} />
    <Route path='week' component={Week} />
    <Route path='week/:day' component={Day} />
    <Route path='clients' component={Clients} />
  </Route>
)
