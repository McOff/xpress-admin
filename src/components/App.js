import React from 'react';
import Sidebar from './Sidebar/Sidebar';

export default (props) => {
  return (
    <div className="app">
      <Sidebar />
      {props.children}
    </div>
  );
}
