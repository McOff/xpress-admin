import React from 'react';
import { Link } from 'react-router';
import Plus from 'react-icons/lib/fa/plus-circle';
import ListUI from 'react-icons/lib/fa/list-ul';


export default (props) => {
  return (
    <div className="options">
      <div className="title">
        <Link to="/admin/week">
            <h2>12 неделя</h2>
            <p>20 мар - 26 мар</p>
        </Link>
      </div>
      <ul className="week-days">
        <li><Link to="/admin/week/mon" activeClassName="active">Понедельник</Link></li>
        <li><Link to="/admin/week/two" activeClassName="active">Вторник</Link></li>
        <li><Link to="/admin/week/wen" activeClassName="active">Среда</Link></li>
        <li><Link to="/admin/week/thirs" activeClassName="active">Четверг</Link></li>
        <li><Link to="/admin/week/rf" activeClassName="active">Пятница</Link></li>
        <li><Link to="/admin/week/sat" activeClassName="active">Суббота</Link></li>
        <li><Link to="/admin/week/son" activeClassName="active">Воскресенье</Link></li>
      </ul>
      <button className="add">
        <div className="btn-text">Добавить<br/>занятие</div>
        <div className="btn-icon"><Plus className="icon-opt"/></div>
      </button>

      <button className="add">
        <Link to="/admin/week/archive">
          <div className="btn-text">Все Недели</div>
          <div className="btn-icon"><ListUI /></div>
        </Link>
     </button>
    </div>
  )
}
