import React, { Component } from 'react';
import SidebarMenu from './SidebarMenu';
import SidebarOptions from './SidebarOptions';



class Sidebar extends Component {
  render() {
    return (
      <div className="sidebar">
        <SidebarMenu />
        <SidebarOptions />
      </div>
    )
  }
}

export default Sidebar;
