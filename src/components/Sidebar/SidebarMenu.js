import React from 'react';
import { Link, IndexLink } from 'react-router';
import Calendar from 'react-icons/lib/fa/calendar';
import Group from 'react-icons/lib/fa/group';

export default (props) => {
  return (
    <ul className="sidebar-menu">
      <li>
        <Link to="/admin/week" className="active"><Calendar className="side-icon"/><p>Расписание</p></Link>
      </li>
      <li>
        <Link to="/admin/clients" activeClassName="active"><Group className="side-icon"/><p>Клиенты</p></Link>
      </li>
    </ul>
  )
}
