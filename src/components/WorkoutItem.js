import React from 'react';
import Plus from 'react-icons/lib/fa/plus-circle';

 const WorkoutItem = ({ title, startTime, instructor, price, notes, numOfClients }) => {
  return (
    <tr>
      <td className="start-time">
        <span>{startTime}</span>
      </td>
      <td>
        <div className="workout-item">
          <div className="workout-info">
            <h3>{title}</h3>
            <p>{instructor}</p>
            <p>{price}</p>
            <p>{notes}</p>
          </div>
          <div className="workout-clients">
            <h3>Клиенты ({numOfClients}) <Plus className="icon-add"/></h3>
            <ul className="clients">
              <li>Петрова О.</li>
              <li>Петрова О.</li>
              <li>Петрова О.</li>
              <li>Петрова О.</li>
            </ul>
          </div>
        </div>
      </td>
    </tr>
  )
}

export default WorkoutItem;
