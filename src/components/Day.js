import React, { Component } from 'react';
import WorkoutItem from 'WorkoutItem';


class Day extends Component {
  render() {
    return (
      <div className="day">
        <h2>Понедельник</h2>
        <table className="workouts">
          <thead>
            <tr>
              <th className="start-time">
                Начало
              </th>
              <th>
                Занятие
              </th>
            </tr>
          </thead>
          <tbody >
            <WorkoutItem {...this.props.workouts[0]} />
          </tbody>
        </table>
      </div>
    );
  }
}

Day.defaultProps = {
  workouts: [{
    title: 'Табата',
    startTime: '11:00',
    instructor: 'Татьяна',
    price: '0 руб',
    notes: 'Макс. 5 чел.',
    numOfClients: '11'
  }]
};

export default Day;
